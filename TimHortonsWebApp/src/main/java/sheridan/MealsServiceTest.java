package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test //Regular test
	public void testDrinksRegular() {
		
		MealsService m = new MealsService();
		assertFalse("List has no value",m.getAvailableMealTypes(MealType.DRINKS).isEmpty());
		System.out.println(m.getAvailableMealTypes(MealType.DRINKS));
		
	}
	
	@Test //Exception(negative)
	public void testDrinksException() {
		MealsService m = new MealsService();
		
		assertTrue("List has no Value",m.getAvailableMealTypes(null).contains("No Brand Available"));
		
		System.out.println(m.getAvailableMealTypes(null));
	}
	
	@Test //BoundaryIn
	public void testDrinksBoundaryIn() {
		MealsService m = new MealsService();
		assertTrue("List has more than 3 elements", m.getAvailableMealTypes(MealType.DRINKS).size() >=3 );
	}
	
	@Test //BoundaryOut
	public void testDrinksBoundaryOut() {
		MealsService m = new MealsService();
		assertTrue("List has more than 1 elements",m.getAvailableMealTypes(null).size()<=1);
	}

}
